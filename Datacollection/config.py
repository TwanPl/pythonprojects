# config.py
from os import environ

class Config:
    #settings flask app
    #general Config options
    SECRET_KEY = environ.get('SECRET_KEY')
    FLASK_APP = environ.get('FLASK_APP')
    FLASK_ENV = environ.get('FLASK_ENV')

    #session id
    SESSION_TYPE = environ.get('SESSION_TYPE')

# Enable Flask's debugging features. Should be False in production
DEBUG = True