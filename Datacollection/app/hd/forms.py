from flask_wtf import FlaskForm
from wtforms import StringField, TextField, SubmitField
from wtforms.validators import DataRequired, Length


class CompanyInfoForm(FlaskForm):
    # Form to use a ticker or name ot ask for info
    name = StringField('Name', [DataRequired(), Length(min=2, message=('Input is to short'))])
    submit = SubmitField('Submit')