from flask import render_template, request, Blueprint, redirect, url_for
from app.hd.forms import CompanyInfoForm

hd = Blueprint('hd', __name__)


@hd.route("/hisdata", methods=('GET', 'POST'))
def hisdata():
    form = CompanyInfoForm()
    if form.validate_on_submit():
        return redirect(url_for('succes'))
    return render_template('historicdata.html', title='Historic data', form=form)
