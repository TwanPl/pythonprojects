# app/__init__.py

from flask import Flask
import sqlalchemy

db = sqlalchemy()

# Initialize the app
app = Flask(__name__, instance_relative_config=True)
# app config
app.config.from_object(config.Config)

# Load the views
from app.main.routes import main
from app.hd.routes import hd

app.register_blueprint(main)
app.register_blueprint(hd)
# Load the config file
app.config.from_object('config')
