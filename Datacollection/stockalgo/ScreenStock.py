import datetime as dt
import pandas as pd
from pandas_datareader import  data as pdr
import yfinance as yf

yf.pdr_override()

startyear = 2018
startmonth = 1
startday = 1

start = dt.datetime(startyear, startmonth, startday)