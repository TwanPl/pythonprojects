import datetime as dt
import pandas as pd
from pandas_datareader import data as pdr
import yfinance as yf

# ticker
msft = yf.Ticker("MSFT")

msft_historical = msft.history(start="2020-08-02", end="2020-08-07", interval="1m")

print(msft_historical)