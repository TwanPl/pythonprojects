import pandas as pd
import mplfinance as mpf
import numpy as np
import datetime as dt
import pandas_datareader as pdr
import yfinance as yf
import flask
import plotly as pl

# Data import hd
yf.pdr_override()
# input stock code
stock = input("enter Ticker symbol: ")

# seach start year
yearstart = 2019
# seach start month
monthstart = 1
# seach start day
daystart = 1
# Start datetime for search
start = dt.datetime(yearstart, monthstart, daystart)
# end day
now = dt.datetime.now()
# start dataframe
df = pdr.get_data_yahoo(stock, start, now)
# time in days
ma = 50
# var
smaString = "Sma_" + str(ma)
# moving Average qqq
df[smaString] = df.iloc[:, 5].rolling(window=ma).mean()
df = df.iloc[ma:]
print(smaString)
# print(df[smaString])
print(df)
# for loop for comparing Moving Average with Adj Closed
numH = 0
numC = 0

for i in df.index:
    if (df["Adj Close"][i]) > df[smaString][i]:
        print("The close is higher")
        numH += 1
    else:
        print("The close is lower")
        numC += 1

print(str(numH))
print(str(numC))
