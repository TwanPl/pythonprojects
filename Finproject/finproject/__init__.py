# imports
from flask import Flask
from flask_wtf import CSRFProtect
from finproject.config import Config
# app config file
# app.config.from_object()

def create_app(config=Config):
    # initialuize finproject
    app = Flask(__name__, instance_relative_config=True)
    csrf = CSRFProtect()
    print(csrf)
    app.config.from_object(Config)
    # load blueprints
    from finproject.home.routes import main
    from finproject.hd.routes import hd

    app.register_blueprint(main)
    app.register_blueprint(hd)
    # load the config file
    return app
