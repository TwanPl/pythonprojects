from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length, ValidationError


class CompanyInfoForm(FlaskForm):
    # Form to use a ticker or name ot ask for info
    name = StringField('Name', validators=[DataRequired(), Length(min=2)])
    submit = SubmitField('Find Data')
