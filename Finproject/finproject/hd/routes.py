from flask import render_template, request, Blueprint, redirect, url_for, flash
from finproject.hd.forms import CompanyInfoForm
import yfinance as yh

hd = Blueprint('hd', __name__)


@hd.route("/hisdata", methods=('GET', 'POST'))
def hisdata():
    form = CompanyInfoForm()
    if form.validate_on_submit():
        test = CompanyInfoForm(form.name.data)
        #ompany = testk
        flash(str(test), 'info')
        return redirect(url_for('hd.hisdata'))
    return render_template('historicdata.html', title='Historic data', form=form)
