# import
import os

# config options
class Config:
    # SECRET_KEY = os.environ.get('ertasf sadf')
    SECRET_KEY = 'flask-session-insecure-secret-key'
    WTF_CSRF_SECRET_KEY = 'secretKey-213'
    WTF_CSRF_ENABLED = False
    # app.config['WTF_CSRF_SECRET_KEY'] = "secretkey"
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
