from flask import render_template, request, Blueprint

front = Blueprint('front', __name__)


@front.route("/")
@front.route("/home")
def home():
    return render_template('home.html')
