from flask import Flask

from fin.config import Config


#db = SQLAlchemy()
#bcrypt = Bcrypt()
#login_manager = LoginManager()
#login_manager.login_view = 'users.login'
#login_manager.login_message_category = 'info'
#mail = Mail()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)

#   db.init_app(app)
#   bcrypt.init_app(app)
#   login_manager.init_app(app)
#   mail.init_app(app)

    from fin.front import routes
#   from flaskblog.posts.routes import posts
#   from flaskblog.main.routes import main
    app.register_blueprint(front)

    return app